%MATLAB 2020b
%name: peakAnalysis
%author: blazej_marszalek
%date: 06.12.2020
%version: 1.0

%load specified part of data from .dat file
fid = fopen('Co60_1350V_x20_p7_sub.dat', 'r');
fseek(fid, 50, 'bof');
y = fread(fid, 10000, 'uint32');
fclose(fid);

T = 5E-10 %[s] sampling period

%X-axis
x = T:T:height(y)*T

%plot
plot(x, y)

xlabel('time [s]')
ylabel('voltage [V]')

%saving created plot to pdf
saveas(plot,'Co60_1350V_x20_p7_sub_histogram.pdf')
